# -*- coding: utf-8 -*-
"""
A collection of deterministic models for infection diseases. Mirrors PyRoss.
To be used with scipy.integrate and pymc3.ode
"""
import numpy as np


def SIR(t,y: np.array,parameters):
    """
    SIR model. All SIR normalized such that \sum_i S_i+I_i+R_i = 1
    Ia = asumptomatic infective
    Is = symptomatic infectives

    Parameters
    ----------
    t : float
        current time
    y : np.array
        current state [S, Ia, Is]
    **parameters : dict
        SIR parameters
        alpha: fraction asymptomatic
        beta: infection rate
        gIa: recovery rate of asymptomatic
        gIs: recovery rate of symptomatic
        fsa: self isolation parameter

    Returns
    -------
    dy/dt

    """
    alpha, beta, contactMatrix, fsa, gIa, gIs =\
    list(map(parameters.get, \
             ['alpha','beta', 'contactMatrix', 'fsa', 'gIa', 'gIs']) )
    S, Ia, Is = np.split(y, len(y))
    fi = np.sum(y, axis=0)
    It = np.asarray(Ia+fsa*Is).flatten()
    # print(It)
    bb = ((beta*contactMatrix(t)@It).T/fi).T
    aa = bb*S
    dS = -aa
    dIa = alpha*aa - gIa*Ia
    dIs = (1-alpha)*aa - gIs*Is
    return np.array([dS,dIa,dIs]).flatten()


def SIRS(t,y: np.array,parameters):
    """
    SIRS model. All SIR normalized such that \sum_i S_i+I_i+R_i = 1
    Ia = asumptomatic infective
    Is = symptomatic infectives

    Parameters
    ----------
    t : float
        current time
    y : np.array
        current state [S, Ia, Is, Ni]
    **parameters : dict
        SIRS parameters
        alpha: fraction asymptomatic
        beta: infection rate
        gIa: recovery rate of asymptomatic
        gIs: recovery rate of symptomatic
        fsa: self isolation parameter
        ep: fraction who become susceptible again
        sa: daily arrival rate of susceptables
        isa: daily arrival rate of asymptomatics

    Returns
    -------
    dy/dt

    """
    alpha, beta, contactMatrix, fsa, gE, gIa, gIs, ep, sa, iaa =\
    list(map(parameters.get, \
             ['alpha','beta', 'contactMatrix', 'fsa', 'gE', 'gIa', 'gIs','ep',
              'sa','iaa']) )

    S, Ia, Is, Ni = np.split(y, len(y))
    fi = np.sum(y, axis=0)
    It = np.asarray(Ia+fsa*Is).flatten()
    M = len(S)
    
    if sa is None:
        sa=np.zeros(M) 
    elif np.size(sa)==1:
        sa = sa*np.ones(M)
    elif np.size(sa)==M:
        sa= sa
    else:
        print('sa can be a number or an array of size M')
       
    if iaa is None:
        iaa=np.zeros(M)
    elif np.size(iaa)==1:
        iaa = iaa*np.ones(M)
    elif np.size(iaa)==M:
        iaa = iaa
    else:
        print('iaa can be a number or an array of size M')
        iaa   = np.zeros(M)
    # print(It)
    bb = ((beta*contactMatrix(t)@It).T/fi).T
    aa = bb*S
    dS = -aa + sa + ep*(gIa*Ia + gIs*Is) 
    dIa = alpha*aa - gIa*Ia +iaa
    dIs = (1-alpha)*aa - gIs*Is
    dNi = sa +iaa
    return np.array([dS,dIa,dIs, dNi]).flatten()


def SEIR(t,y: np.array,parameters):
    """
    SEIR model. All SIR normalized such that \sum_i S_i+E_i+I_i+R_i = 1
    Ia = asumptomatic infective
    Is = symptomatic infectives

    Parameters
    ----------
    t : float
        current time
    y : np.array
        current state [S,E, Ia, Is]
    **parameters : dict
        SEIR parameters
        alpha: fraction asymptomatic
        beta: infection rate
        gE: recovery rate of E
        gIa: recovery rate of asymptomatic
        gIs: recovery rate of symptomatic
        fsa: self isolation parameter
        

    Returns
    -------
    dy/dt

    """
    alpha, beta, contactMatrix, fsa, gE, gIa, gIs =\
    list(map(parameters.get, \
             ['alpha','beta', 'contactMatrix', 'fsa', 'gE', 'gIa', 'gIs']) )
    S, E, Ia, Is = np.split(y, len(y))
    fi = np.sum(y, axis=0)
    It = np.asarray(Ia+fsa*Is).flatten()
    # print(It)
    bb = ((beta*contactMatrix(t)@It).T/fi).T
    aa = bb*S
    dS = -aa
    dE = aa - gE*E
    dIa = alpha*gE*E - gIa*Ia
    dIs = (1-alpha)*gE*E - gIs*Is
    return np.array([dS,dE,dIa,dIs]).flatten()


if __name__=='__main__':
    def contactMatrix(t):
        return np.identity(2)
    parameters = {'alpha':.5,'beta': 1, 'contactMatrix': contactMatrix, 'fsa':0.5,
                  'gE':.001,'gIa':.1, 'gIs':.1, 'ep':.1}
    # print(**parameters)
    # SIR(0,np.array([.99,.01,0])parameters)
    print(SIR(0,np.array([[.5,.46],[0,.01],[0,0]]),parameters))
    # print(np.array([[2]])@np.array([1]))
    # print(SEIR(0,np.array([.9,.1,0.1,0]), parameters))
    
    # print(SIRS(0,np.array([.9,.1,0.1]), parameters))


