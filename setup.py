# -*- coding: utf-8 -*-
import os
from distutils.core import setup
import codecs

# shamelessly copied from pseudopy which was shamelessly copied from VoroPy
def read(fname):
    return codecs.open(os.path.join(os.path.dirname(__file__), fname), encoding='utf-8').read()

setup(name='pymcross',
      packages=['pymcross'],
      version='0.0.1',
      description='A pure implementation of PyRoss using Pymc3 for benchmarking purposes. ',
      long_description=read('README.md'),
      author='Austen Bolitho',
      author_email='ab2075@cam.ac.uk',
      url='https://gitlab.com/AustenBolitho/pymcross',
      install_requires=['matplotlib', 'numpy',
                'scipy','theano','pymc3'],
      classifiers=[
          'Development Status :: 1 - alpha',
          'Intended Audience :: Science/Research',
          'License :: OSI Approved :: MIT License',
          'Operating System :: OS Independent',
          'Programming Language :: Python',
          'Programming Language :: Python :: 3',
          'Topic :: Scientific/Engineering :: Mathematics'
          ],
      )