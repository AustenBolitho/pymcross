# -*- coding: utf-8 -*-
"""
Testing against deterministic models in pyross
"""
import pymcross.models.deterministic as pmcd
import pyross.deterministic as prd
import scipy.integrate as spi
import numpy as np
import unittest
import inspect

#generic params to be used by all models
M = 1
Ni=1000*np.ones(M)
N=np.sum(Ni)
Tf = 100;  Nt=100; 
Ia0 = np.array([0])     # the SIR model has only one kind of infective 
Is0 = np.array([1])     # we take these to be symptomatic 
R0  = np.array([0])     # and assume there are no recovered individuals initially 
E0 = np.array([0])
S0  = N-(Ia0+Is0+R0)    # so that the initial susceptibles are obtained from S + Ia + Is + R = N

def contactMatrix(t):
        return np.identity(M)
parameters = {'alpha':.5,'beta': 1, 'contactMatrix': contactMatrix, 'fsa':0.5,
                  'gE':.1,'gIa':.1, 'gIs':.1, 'ep':.1, 'sa':.1, 'iaa':.1}


# def run_pyross_model(model, y0):
#     data = model(parameters, M, Ni).simulate(S0, Ia0, Is0, contactMatrix, Tf, Nt)
#     return data

# def run_pymcross_model(model, y0):
#     time_points=np.linspace(0, Tf, Nt)
#     y0=y0
#     data = spi.solve_ivp(model,(0,Tf), y0,t_eval = time_points, 
#                           args=[parameters],atol=1E-7, rtol=1E-6)
#     return data


class TestDeterministicModels(unittest.TestCase):
    """
    Test pyross models against pymcross
    """
    def __init__(self, *args, **kwargs):
        super(TestDeterministicModels, self).__init__(*args, **kwargs)
        self.pmcd_models =  dict(inspect.getmembers(pmcd, inspect.isfunction))
        self.pyross_models =  dict(inspect.getmembers(prd, inspect.isclass))

    def test_subset(self):
        """
        test that pymcross is a subset of pyross
        """
        pmcd_names, pyross_names=self.pmcd_models.keys(),self.pyross_models.keys()
        self.assertTrue(set(pmcd_names).issubset(set(pyross_names)))
        print(f"\n Found models {list(pmcd_names)}")
        
    def test_SIR(self):
        """
        integrate SIR and compare mean square difference
        """
        y0 = np.array([S0,Ia0,Is0]).flatten()
        time_points=np.linspace(0, Tf, Nt)
        pmcd_run = spi.solve_ivp(self.pmcd_models["SIR"],(0,Tf), 
                                 y0,t_eval = time_points, args=[parameters],
                                 atol=1E-7, rtol=1E-6)
        pyross_run = self.pyross_models["SIR"](
            parameters, M, Ni).simulate(
                S0, Ia0, Is0, contactMatrix, Tf, Nt)
        sumsq = np.sqrt(np.sum( (pmcd_run.y.T-pyross_run["X"])**2))/(N*Nt)
        self.assertTrue(sumsq<0.1)
        print(f"\n SIR integration agrees within .1%")
        
    def test_SEIR(self):
        """
        integrate SEIR and compare mean square difference
        """
        y0 = np.array([S0,E0,Ia0,Is0]).flatten()
        time_points=np.linspace(0, Tf, Nt)
        pmcd_run = spi.solve_ivp(self.pmcd_models["SEIR"],(0,Tf), 
                                 y0,t_eval = time_points, args=[parameters],
                                 atol=1E-7, rtol=1E-6)
        pyross_run = self.pyross_models["SEIR"](
            parameters, M, Ni).simulate(
                S0,E0, Ia0, Is0, contactMatrix, Tf, Nt)
        sumsq = np.sqrt(np.sum( (pmcd_run.y.T-pyross_run["X"])**2))/(N*Nt)
        self.assertTrue(sumsq<0.1)
        print(f"\n SEIR integration agrees within .1%")
        
    def test_SIRS(self):
        """
        integrate SIRS and compare mean square difference
        """
        y0 = np.array([S0,Ia0,Is0,Ni]).flatten()
        time_points=np.linspace(0, Tf, Nt)
        pmcd_run = spi.solve_ivp(self.pmcd_models["SIRS"],(0,Tf), 
                                 y0,t_eval = time_points, args=[parameters],
                                 atol=1E-7, rtol=1E-6)
        pyross_run = self.pyross_models["SIRS"](
            parameters, M, Ni).simulate(
                S0,Ia0, Is0, contactMatrix, Tf, Nt)
        sumsq = np.sqrt(np.sum( (pmcd_run.y.T-pyross_run["X"])**2))/(N*Nt)
        self.assertTrue(sumsq<0.1)
        print(f"\n SIRS integration agrees within .1%")
            
        
        
        
    


if __name__=='__main__':
    unittest.main()